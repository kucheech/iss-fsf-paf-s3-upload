(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http", "$log"];

    function MyAppCtrl($http) {
        var vm = this; // vm
        vm.file = "01.jpg";

        vm.uploadImage = function() {
            console.log(vm.file);
        }

        vm.initForm = function () {
            $http.get("/hello")
                .then(function (result) {
                    console.log(result);
                    vm.reply = result.data;
                }).catch(function (e) {
                    console.log(e);
                });
        };

        vm.initForm();
    }

})();