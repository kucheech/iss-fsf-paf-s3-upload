(function () {
    "use strict";
    angular.module("MyApp").controller("EditUserCtrl", EditUserCtrl);

    EditUserCtrl.$inject = ["MyAppService", "$state"];

    function EditUserCtrl(MyAppService, $state) {
        var self = this; // vm
        self.book = null;
        self.id = 0;

        self.init = function () {
            // console.log($state.params.id);
            self.id = $state.params.id;
            // self.showResults = false;

            MyAppService.getUserById(self.id)
                .then(function (result) {
                    console.log(result);
                    self.user = result;
                }).catch(function (err) {
                    console.log(err);
                });

        }

        self.init();

        self.cancelEdit = function() {
            $state.go("users");
        }

        self.saveEdit = function() {
            MyAppService.updateUser(self.user)
            .then(function (result) {
                console.log(result);
                $state.go("users");
            }).catch(function (err) {
                console.log(err)
            });
        }

    }

})();