(function () {
    "use strict";
    angular.module("MyApp").controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["Upload"];

    function UploadCtrl(Upload) {
        var vm = this; // vm
        vm.imgFile = null;
        vm.status = {
            message: "",
            code: 0
        }

        vm.upload = upload;
        vm.uploadS3 = uploadS3;

        function upload() {
            console.log("upload");
            Upload.upload({
                url: "/upload",
                data: {
                    "img-file": vm.imgFile
                }
            }).then(function (res) {
                console.log(res);
                vm.fileUrl = res.data;
                vm.status.message = "Upload successful";
                vm.status.code = res.status;
            }).catch(function (err) {
                vm.status.message = "Failed";
                console.log(err);
            });
        }

        function uploadS3() {
            console.log("uploadS3");
            Upload.upload({
                url: "/uploadS3",
                data: {
                    "img-file": vm.imgFile
                }
            }).then(function (res) {
                vm.fileUrl = res.data;
                vm.status.message = "Upload successful";
                vm.status.code = res.status;
            }).catch(function (err) {
                vm.status.message = "Failed";
                console.log(err);
            });
        };


    }

})();