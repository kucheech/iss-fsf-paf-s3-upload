require('dotenv').config();
// console.log(process.env);
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const fs = require("fs");

const NODE_PORT = process.env.PORT || 3000;

const app = express();
app.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));
app.use(bodyParser.json({ limit: "5mb" }));

//routes
require("./aws.js")(app);
require("./routes.js")(app, express, path); //standard


app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

