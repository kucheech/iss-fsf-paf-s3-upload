
module.exports = function(app, express, path) {
    
    const CLIENT_FOLDER = path.join(__dirname, "/../client/");
    app.use(express.static(CLIENT_FOLDER));
    
    const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
    app.use("/libs", express.static(BOWER_FOLDER));
    
    app.get("/hi", function(req, res) {
        res.status(200).send("Hello");
    });
    
    //catch all
    app.use(function (req, res) {
        console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
        res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
    });
}