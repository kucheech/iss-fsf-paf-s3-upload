const aws = require("aws-sdk");
aws.config.update({
    // accessKeyId: process.env.aws_access_key_id,
    // secretAccessKey: process.env.aws_secret_access_key,
    region: "ap-southeast-1"
});
const s3 = new aws.S3();
const multer = require("multer");
const multerS3 = require("multer-s3");
// const uuid = require("uuid/v4");

const uploadS3 = new multer({
    storage: multerS3({
        s3: s3,
        bucket: "nus-iss-fsf",
        metadata: function (req, file, cb) {
            console.log("file.fieldname: " + file.fieldname);
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            // console.log(file);
            var data = file.mimetype.split("/");
            const ext = "." + data[1];
            cb(null, Date.now() + ext);
        }
    })
});

module.exports = function (app) {
    app.post("/uploadS3", uploadS3.single("img-file"), function (req, res) {
        console.log("uploadS3");
        res.status(202).send("File sent successfully");
    });
}
